<?php
/**
 * @file
 * bobi_ecommerce.features.inc
 */

/**
 * Implements hook_views_api().
 */
function bobi_ecommerce_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_uc_product_default_classes().
 */
function bobi_ecommerce_uc_product_default_classes() {
  $items = array(
    'product' => array(
      'name' => t('Product'),
      'base' => 'uc_product',
      'description' => t('Use <em>products</em> to represent items for sale on the website, including all the unique information that can be attributed to a specific model number.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'product_card' => array(
      'name' => t('Card'),
      'base' => 'uc_product',
      'description' => t('Use this content type to create product Card. For example: Valentine cards, happy birthday cards...'),
      'has_title' => '1',
      'title_label' => t('Card title'),
      'help' => '',
    ),
    'product_gift' => array(
      'name' => t('Gift'),
      'base' => 'uc_product',
      'description' => t('Use this content type to create product Gifts. For example: soap'),
      'has_title' => '1',
      'title_label' => t('Gift name'),
      'help' => '',
    ),
  );
  return $items;
}
