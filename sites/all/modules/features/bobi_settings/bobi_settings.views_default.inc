<?php
/**
 * @file
 * bobi_settings.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function bobi_settings_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'bobi_carousel';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Bobi Carousel';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_carousel_image' => 'field_carousel_image',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'carousel' => 'carousel',
  );

  /* Display: Carousel block */
  $handler = $view->new_display('block', 'Carousel block', 'block_carousel');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'bobi_featured';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['wrapper_class'] = 'tab-handlers';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<a href="#">[title]</a>';
  $handler->display->display_options['fields']['title']['alter']['path'] = '#';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'carousel' => 'carousel',
  );

  /* Display: [Attachment] Carousel images */
  $handler = $view->new_display('attachment', '[Attachment] Carousel images', 'attachment_1');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'tab-fragments';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Target */
  $handler->display->display_options['fields']['field_carousel_target']['id'] = 'field_carousel_target';
  $handler->display->display_options['fields']['field_carousel_target']['table'] = 'field_data_field_carousel_target';
  $handler->display->display_options['fields']['field_carousel_target']['field'] = 'field_carousel_target';
  $handler->display->display_options['fields']['field_carousel_target']['label'] = '';
  $handler->display->display_options['fields']['field_carousel_target']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_carousel_target']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_target']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_carousel_target']['type'] = 'link_absolute';
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_carousel_image']['id'] = 'field_carousel_image';
  $handler->display->display_options['fields']['field_carousel_image']['table'] = 'field_data_field_carousel_image';
  $handler->display->display_options['fields']['field_carousel_image']['field'] = 'field_carousel_image';
  $handler->display->display_options['fields']['field_carousel_image']['label'] = '';
  $handler->display->display_options['fields']['field_carousel_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_carousel_image']['alter']['path'] = '[field_carousel_target]';
  $handler->display->display_options['fields']['field_carousel_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_carousel_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['displays'] = array(
    'block_carousel' => 'block_carousel',
    'default' => 0,
  );
  $handler->display->display_options['attachment_position'] = 'after';
  $translatables['bobi_carousel'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Carousel block'),
    t('<a href="#">[title]</a>'),
    t('[Attachment] Carousel images'),
  );
  $export['bobi_carousel'] = $view;

  $view = new view();
  $view->name = 'bobi_feature_cards';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Bobi Feature Cards';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Thiệp valentine';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Product: Add to cart form */
  $handler->display->display_options['fields']['addtocartlink']['id'] = 'addtocartlink';
  $handler->display->display_options['fields']['addtocartlink']['table'] = 'uc_products';
  $handler->display->display_options['fields']['addtocartlink']['field'] = 'addtocartlink';
  $handler->display->display_options['fields']['addtocartlink']['label'] = '';
  $handler->display->display_options['fields']['addtocartlink']['element_label_colon'] = FALSE;
  /* Field: Content: Large image */
  $handler->display->display_options['fields']['field_card_large_image']['id'] = 'field_card_large_image';
  $handler->display->display_options['fields']['field_card_large_image']['table'] = 'field_data_field_card_large_image';
  $handler->display->display_options['fields']['field_card_large_image']['field'] = 'field_card_large_image';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'product_card' => 'product_card',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = '<none>';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Product: Add to cart form */
  $handler->display->display_options['fields']['addtocartlink']['id'] = 'addtocartlink';
  $handler->display->display_options['fields']['addtocartlink']['table'] = 'uc_products';
  $handler->display->display_options['fields']['addtocartlink']['field'] = 'addtocartlink';
  $handler->display->display_options['fields']['addtocartlink']['label'] = '';
  $handler->display->display_options['fields']['addtocartlink']['exclude'] = TRUE;
  $handler->display->display_options['fields']['addtocartlink']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['addtocartlink']['element_wrapper_class'] = 'bbicon';
  /* Field: Content: Thumbnail image */
  $handler->display->display_options['fields']['field_card_thumbnail_image']['id'] = 'field_card_thumbnail_image';
  $handler->display->display_options['fields']['field_card_thumbnail_image']['table'] = 'field_data_field_card_thumbnail_image';
  $handler->display->display_options['fields']['field_card_thumbnail_image']['field'] = 'field_card_thumbnail_image';
  $handler->display->display_options['fields']['field_card_thumbnail_image']['label'] = '';
  $handler->display->display_options['fields']['field_card_thumbnail_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_card_thumbnail_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_card_thumbnail_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_card_thumbnail_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_card_thumbnail_image']['delta_offset'] = '';
  $handler->display->display_options['fields']['field_card_thumbnail_image']['multi_type'] = 'ul';
  /* Field: Content: Large image */
  $handler->display->display_options['fields']['field_card_large_image']['id'] = 'field_card_large_image';
  $handler->display->display_options['fields']['field_card_large_image']['table'] = 'field_data_field_card_large_image';
  $handler->display->display_options['fields']['field_card_large_image']['field'] = 'field_card_large_image';
  $handler->display->display_options['fields']['field_card_large_image']['label'] = '';
  $handler->display->display_options['fields']['field_card_large_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_card_large_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_card_large_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_card_large_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_card_large_image']['delta_offset'] = '';
  $handler->display->display_options['fields']['field_card_large_image']['multi_type'] = 'ul';
  /* Field: Product: Sell price */
  $handler->display->display_options['fields']['sell_price']['id'] = 'sell_price';
  $handler->display->display_options['fields']['sell_price']['table'] = 'uc_products';
  $handler->display->display_options['fields']['sell_price']['field'] = 'sell_price';
  $handler->display->display_options['fields']['sell_price']['label'] = '';
  $handler->display->display_options['fields']['sell_price']['exclude'] = TRUE;
  $handler->display->display_options['fields']['sell_price']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['sell_price']['precision'] = '0';
  /* Field: Customize link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['ui_name'] = 'Customize link';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['exclude'] = TRUE;
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['text'] = 'Thêm lời nhắn gửi';
  /* Field: Global text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Global text';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="item">
            <div class="maincol">
               [field_card_large_image]
               <span class="ribbon">[title] / [sell_price]</span>
                <div class="actions">
                    [addtocartlink]
                    <span class="customize">[view_node]</span>
                </div>
            </div>
            <div class="sidecol">
               [field_card_thumbnail_image]
            </div>
        </div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['path'] = 'thiep';
  $translatables['bobi_feature_cards'] = array(
    t('Master'),
    t('Thiệp valentine'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Large image'),
    t('Page'),
    t('<none>'),
    t('.'),
    t(','),
    t('Thêm lời nhắn gửi'),
    t('<div class="item">
            <div class="maincol">
               [field_card_large_image]
               <span class="ribbon">[title] / [sell_price]</span>
                <div class="actions">
                    [addtocartlink]
                    <span class="customize">[view_node]</span>
                </div>
            </div>
            <div class="sidecol">
               [field_card_thumbnail_image]
            </div>
        </div>'),
  );
  $export['bobi_feature_cards'] = $view;

  $view = new view();
  $view->name = 'bobi_feature_gifts';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Bobi Feature Gifts';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Quà tặng Valentine';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'product_gift' => 'product_gift',
  );

  /* Display: Page Gifts */
  $handler = $view->new_display('page', 'Page Gifts', 'page');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Product: Add to cart form */
  $handler->display->display_options['fields']['addtocartlink']['id'] = 'addtocartlink';
  $handler->display->display_options['fields']['addtocartlink']['table'] = 'uc_products';
  $handler->display->display_options['fields']['addtocartlink']['field'] = 'addtocartlink';
  $handler->display->display_options['fields']['addtocartlink']['label'] = '';
  $handler->display->display_options['fields']['addtocartlink']['exclude'] = TRUE;
  $handler->display->display_options['fields']['addtocartlink']['element_label_colon'] = FALSE;
  /* Field: Content: Large image */
  $handler->display->display_options['fields']['field_gift_large_image']['id'] = 'field_gift_large_image';
  $handler->display->display_options['fields']['field_gift_large_image']['table'] = 'field_data_field_gift_large_image';
  $handler->display->display_options['fields']['field_gift_large_image']['field'] = 'field_gift_large_image';
  $handler->display->display_options['fields']['field_gift_large_image']['label'] = '';
  $handler->display->display_options['fields']['field_gift_large_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_gift_large_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_gift_large_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_gift_large_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_gift_large_image']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_gift_large_image']['multi_type'] = 'ul';
  /* Field: Content: Thumbnail image */
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['id'] = 'field_gift_thumbnail_image';
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['table'] = 'field_data_field_gift_thumbnail_image';
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['field'] = 'field_gift_thumbnail_image';
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['label'] = '';
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['multi_type'] = 'ul';
  /* Field: Product: Sell price */
  $handler->display->display_options['fields']['sell_price']['id'] = 'sell_price';
  $handler->display->display_options['fields']['sell_price']['table'] = 'uc_products';
  $handler->display->display_options['fields']['sell_price']['field'] = 'sell_price';
  $handler->display->display_options['fields']['sell_price']['label'] = '';
  $handler->display->display_options['fields']['sell_price']['exclude'] = TRUE;
  $handler->display->display_options['fields']['sell_price']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['sell_price']['precision'] = '0';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="item">
            <div class="maincol">
               [field_gift_large_image]
               <span class="ribbon">[title] / [sell_price]</span>
                <div class="actions">
                    [addtocartlink]
                    <!--<span class="customize">[view_node]</span>-->
                </div>
            </div>
            <div class="sidecol">
               [field_gift_thumbnail_image]
            </div>
        </div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'product_gift' => 'product_gift',
  );
  /* Filter criterion: Content: Category (field_gift_category) */
  $handler->display->display_options['filters']['field_gift_category_value']['id'] = 'field_gift_category_value';
  $handler->display->display_options['filters']['field_gift_category_value']['table'] = 'field_data_field_gift_category';
  $handler->display->display_options['filters']['field_gift_category_value']['field'] = 'field_gift_category_value';
  $handler->display->display_options['filters']['field_gift_category_value']['value'] = array(
    'gift' => 'gift',
  );
  $handler->display->display_options['path'] = 'qua-tang';

  /* Display: Page Decor */
  $handler = $view->new_display('page', 'Page Decor', 'page_1');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Product: Add to cart form */
  $handler->display->display_options['fields']['addtocartlink']['id'] = 'addtocartlink';
  $handler->display->display_options['fields']['addtocartlink']['table'] = 'uc_products';
  $handler->display->display_options['fields']['addtocartlink']['field'] = 'addtocartlink';
  $handler->display->display_options['fields']['addtocartlink']['label'] = '';
  $handler->display->display_options['fields']['addtocartlink']['exclude'] = TRUE;
  $handler->display->display_options['fields']['addtocartlink']['element_label_colon'] = FALSE;
  /* Field: Content: Large image */
  $handler->display->display_options['fields']['field_gift_large_image']['id'] = 'field_gift_large_image';
  $handler->display->display_options['fields']['field_gift_large_image']['table'] = 'field_data_field_gift_large_image';
  $handler->display->display_options['fields']['field_gift_large_image']['field'] = 'field_gift_large_image';
  $handler->display->display_options['fields']['field_gift_large_image']['label'] = '';
  $handler->display->display_options['fields']['field_gift_large_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_gift_large_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_gift_large_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_gift_large_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_gift_large_image']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_gift_large_image']['multi_type'] = 'ul';
  /* Field: Content: Thumbnail image */
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['id'] = 'field_gift_thumbnail_image';
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['table'] = 'field_data_field_gift_thumbnail_image';
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['field'] = 'field_gift_thumbnail_image';
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['label'] = '';
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_gift_thumbnail_image']['multi_type'] = 'ul';
  /* Field: Product: Sell price */
  $handler->display->display_options['fields']['sell_price']['id'] = 'sell_price';
  $handler->display->display_options['fields']['sell_price']['table'] = 'uc_products';
  $handler->display->display_options['fields']['sell_price']['field'] = 'sell_price';
  $handler->display->display_options['fields']['sell_price']['label'] = '';
  $handler->display->display_options['fields']['sell_price']['exclude'] = TRUE;
  $handler->display->display_options['fields']['sell_price']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['sell_price']['precision'] = '0';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="item">
            <div class="maincol">
               [field_gift_large_image]
               <span class="ribbon">[title] / [sell_price]</span>
                <div class="actions">
                    [addtocartlink]
                    <!--<span class="customize">[view_node]</span>-->
                </div>
            </div>
            <div class="sidecol">
               [field_gift_thumbnail_image]
            </div>
        </div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'product_gift' => 'product_gift',
  );
  /* Filter criterion: Content: Category (field_gift_category) */
  $handler->display->display_options['filters']['field_gift_category_value']['id'] = 'field_gift_category_value';
  $handler->display->display_options['filters']['field_gift_category_value']['table'] = 'field_data_field_gift_category';
  $handler->display->display_options['filters']['field_gift_category_value']['field'] = 'field_gift_category_value';
  $handler->display->display_options['filters']['field_gift_category_value']['value'] = array(
    'deco' => 'deco',
  );
  $handler->display->display_options['path'] = 'trang-tri';
  $translatables['bobi_feature_gifts'] = array(
    t('Master'),
    t('Quà tặng Valentine'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Page Gifts'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('.'),
    t(','),
    t('<div class="item">
            <div class="maincol">
               [field_gift_large_image]
               <span class="ribbon">[title] / [sell_price]</span>
                <div class="actions">
                    [addtocartlink]
                    <!--<span class="customize">[view_node]</span>-->
                </div>
            </div>
            <div class="sidecol">
               [field_gift_thumbnail_image]
            </div>
        </div>'),
    t('Page Decor'),
  );
  $export['bobi_feature_gifts'] = $view;

  return $export;
}
