<?php
/**
 * @file
 * bobi_settings.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function bobi_settings_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_trang-ch:<front>
  $menu_links['main-menu_trang-ch:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Trang chủ',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_trang-ch:<front>',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-navigation_cu-chuyn-bobigifts:node/5
  $menu_links['menu-footer-navigation_cu-chuyn-bobigifts:node/5'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => 'node/5',
    'router_path' => 'node/%',
    'link_title' => 'Câu chuyện Bobigifts',
    'options' => array(
      'identifier' => 'menu-footer-navigation_cu-chuyn-bobigifts:node/5',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-footer-navigation_gii-thiu-bobigifts:<front>',
  );
  // Exported menu link: menu-footer-navigation_cu-hi-thng-gp:node/8
  $menu_links['menu-footer-navigation_cu-hi-thng-gp:node/8'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => 'node/8',
    'router_path' => 'node/%',
    'link_title' => 'Câu hỏi thường gặp',
    'options' => array(
      'identifier' => 'menu-footer-navigation_cu-hi-thng-gp:node/8',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-footer-navigation_gii-thiu-bobigifts:<front>',
  );
  // Exported menu link: menu-footer-navigation_gi-hng:cart
  $menu_links['menu-footer-navigation_gi-hng:cart'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => 'cart',
    'router_path' => 'cart',
    'link_title' => 'Giỏ hàng',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-navigation_gi-hng:cart',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-footer-navigation_ti-khon:<front>',
  );
  // Exported menu link: menu-footer-navigation_gii-thiu-bobigifts:<front>
  $menu_links['menu-footer-navigation_gii-thiu-bobigifts:<front>'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Giới thiệu BobiGifts',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-navigation_gii-thiu-bobigifts:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-navigation_lin-h:node/6
  $menu_links['menu-footer-navigation_lin-h:node/6'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => 'node/6',
    'router_path' => 'node/%',
    'link_title' => 'Liên hệ',
    'options' => array(
      'identifier' => 'menu-footer-navigation_lin-h:node/6',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-footer-navigation_gii-thiu-bobigifts:<front>',
  );
  // Exported menu link: menu-footer-navigation_mua-sm:<front>
  $menu_links['menu-footer-navigation_mua-sm:<front>'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Mua sắm',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-navigation_mua-sm:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-navigation_ng-k:user/register
  $menu_links['menu-footer-navigation_ng-k:user/register'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => 'user/register',
    'router_path' => 'user/register',
    'link_title' => 'Đăng kí',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-navigation_ng-k:user/register',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-footer-navigation_ti-khon:<front>',
  );
  // Exported menu link: menu-footer-navigation_ng-nhp:user/login
  $menu_links['menu-footer-navigation_ng-nhp:user/login'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => 'user/login',
    'router_path' => 'user/login',
    'link_title' => 'Đăng nhập',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-navigation_ng-nhp:user/login',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-footer-navigation_ti-khon:<front>',
  );
  // Exported menu link: menu-footer-navigation_thanh-ton-v-vn-chuyn:node/7
  $menu_links['menu-footer-navigation_thanh-ton-v-vn-chuyn:node/7'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => 'node/7',
    'router_path' => 'node/%',
    'link_title' => 'Thanh toán và vận chuyển',
    'options' => array(
      'identifier' => 'menu-footer-navigation_thanh-ton-v-vn-chuyn:node/7',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-footer-navigation_gii-thiu-bobigifts:<front>',
  );
  // Exported menu link: menu-footer-navigation_ti-khon:<front>
  $menu_links['menu-footer-navigation_ti-khon:<front>'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Tài khoản',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-navigation_ti-khon:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-primary-menu_qu-tng:qua-tang
  $menu_links['menu-primary-menu_qu-tng:qua-tang'] = array(
    'menu_name' => 'menu-primary-menu',
    'link_path' => 'qua-tang',
    'router_path' => 'qua-tang',
    'link_title' => 'Quà tặng',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-primary-menu_qu-tng:qua-tang',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-primary-menu_trang-ch:<front>
  $menu_links['menu-primary-menu_trang-ch:<front>'] = array(
    'menu_name' => 'menu-primary-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Trang chủ',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-primary-menu_trang-ch:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-primary-menu_trang-tr:trang-tri
  $menu_links['menu-primary-menu_trang-tr:trang-tri'] = array(
    'menu_name' => 'menu-primary-menu',
    'link_path' => 'trang-tri',
    'router_path' => 'trang-tri',
    'link_title' => 'Trang trí',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-primary-menu_trang-tr:trang-tri',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Câu chuyện Bobigifts');
  t('Câu hỏi thường gặp');
  t('Giỏ hàng');
  t('Giới thiệu BobiGifts');
  t('Liên hệ');
  t('Mua sắm');
  t('Quà tặng');
  t('Thanh toán và vận chuyển');
  t('Trang chủ');
  t('Trang trí');
  t('Tài khoản');
  t('Đăng kí');
  t('Đăng nhập');


  return $menu_links;
}
