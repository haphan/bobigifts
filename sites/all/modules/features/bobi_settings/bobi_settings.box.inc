<?php
/**
 * @file
 * bobi_settings.box.inc
 */

/**
 * Implements hook_default_box().
 */
function bobi_settings_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'box_company_info';
  $box->plugin_key = 'simple';
  $box->title = '<none>';
  $box->description = 'Box Company Info';
  $box->options = array(
    'body' => array(
      'value' => '<h2>BOBI CRAFT CO. LTD</h2>

<p>226A Huynh Van Banh, W.11,&nbsp;<br />
Phu Nhuan, HCMC, Vietnam.</p>

<p>Tel: 08 36024191<br />
Email: info@bobicraft.com</p>
',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['box_company_info'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'box_social_media';
  $box->plugin_key = 'simple';
  $box->title = '<none>';
  $box->description = 'Box Social Media';
  $box->options = array(
    'body' => array(
      'value' => '<div>
<h2>Keep in touch</h2>

<p><a class="twitter" href="#" title="Twitter">Twitter</a> <a class="facebook" href="#" title="Facebook">Facebook</a> <a class="pinterest" href="#" title="Pinterest">Pinterest</a></p>
</div>
',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['box_social_media'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'box_video_tutorial';
  $box->plugin_key = 'simple';
  $box->title = '<none>';
  $box->description = 'Bobi video tutorial';
  $box->options = array(
    'body' => array(
      'value' => '<div><a href="#"><img src="http://placehold.it/310x210.gif" /></a> <a href="#"><img src="http://placehold.it/310x210.gif" /></a> <a class="last" href="#"><img src="http://placehold.it/310x210.gif" /></a></div>
',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['box_video_tutorial'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'copyright';
  $box->plugin_key = 'simple';
  $box->title = '<none>';
  $box->description = 'Bobigifts Footer Copyright';
  $box->options = array(
    'body' => array(
      'value' => '<p><span>© 2014 Bobi Craft. LTD. All rights reserved.</span></p>
',
      'format' => 'wysiwyg_filter',
    ),
    'additional_classes' => '',
  );
  $export['copyright'] = $box;

  return $export;
}
