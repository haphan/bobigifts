var $ = jQuery.noConflict();
$().ready(function(){

    var handleToggler = function(handler, target)
    {


        if(!(handler instanceof jQuery) && !(target instanceof jQuery))
        {
            handler = $(handler);
            target = $(target);
        }


        $(handler).each(function(i,e){
            console.log(e);
            $(e).click(function(){

                if($(target).eq(i)){
                    $(target).hide().eq(i).show();
                    $(handler).show().eq(i).hide();
                }
                return false;
            });
        });
    }

    var handleCarousel = function(handler, target)
    {
        if(!(handler instanceof jQuery) && !(target instanceof jQuery))
        {
            handler = $(handler);
            target = $(target);
        }

        //initialize
        $(handler).eq(0).addClass('active');
        $(target).hide().eq(0).show();

        $(handler).each(function(i,e){
            $(e).click(function(){
                if($(target).eq(i)){
                    $(handler).removeClass('active').eq(i).addClass('active');
                    $(target).hide().eq(i).show();
                }
                return false;
            });
        });
    }

    $('.view-bobi-feature-cards .views-row').each(function(){
        var that = $(this);
        handleToggler($('.sidecol li', that),$('.maincol li', that));
    });

    $('.view-bobi-feature-gifts .views-row').each(function(){
        var that = $(this);
        console.log($('.sidecol li', that));
        handleToggler($('.sidecol li', that),$('.maincol li', that));
    });

    $('.view-bobi-carousel.view-id-bobi_carousel').each(function(){
        var that = $(this);
        console.log($('.tab-fragments .view-content > div', that));
        handleCarousel($('.tab-handlers li', that),$('.tab-fragments .view-content > div', that));
    });


    //Enable lean-slider
    $('.product-slider').each(function(){
        var that = this;
        var slider = $('.slider', that).leanSlider({
            pauseTime: 6000,
            directionNav: '.slider-direction-nav',
            controlNav: '.slider-control-nav'
        });

        var thumbnails = $('.thumbnails', that);
        $('.field-item', thumbnails).each(function(i, e){
            var index = i;
            $(e).click(function(){
                slider.show(index);
            });
        });
    });

});