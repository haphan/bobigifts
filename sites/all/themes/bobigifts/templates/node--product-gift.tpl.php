<article<?php print $attributes; ?>>
  <?php //dpm($content);?>
  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
    <header>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>"
                                              title="<?php print $title ?>"><?php print $title ?></a></h2>
    </header>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($display_submitted): ?>
    <footer class="submitted"><?php print $date; ?> -- <?php print $name; ?></footer>
  <?php endif; ?>

  <div<?php print $content_attributes; ?>>
    <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    //print render($content);
    ?>
    <div class="product-header">
      <div class="product-slider">
        <div class="slider-wrapper">
          <div class="slider">
            <?php if (count($content['field_gift_large_image']['#items']) > 0): ?>
              <?php $imgs = $content['field_gift_large_image']['#items']; ?>
              <?php foreach ($imgs as $i => $img): ?>
                <div class="slide<?php echo $i + 1; ?>">
                  <img src="<?php echo file_create_url($img['uri']) ?>"/>
                </div>
              <?php endforeach; ?>
            <?php endif; ?>
          </div>
          <div class="slider-direction-nav"></div>
          <div class="slider-control-nav"></div>
        </div>
        <div class="thumbnails">
          <?php print render($content['field_gift_thumbnail_image']); ?>
        </div>
      </div>
      <div class="product-add-to-cart">
        <div>
          <?php print render($content['add_to_cart']) ?>
        </div>
      </div>
      <div class="product-short-desc">
        <div>
          <h2><?php print t('Mô tả ngắn gọn về sản phẩm') ?></h2>
          <?php print render($content['body']['#items'][0]['safe_summary']); ?>
        </div>
      </div>
    </div>
    <div class="product-footer">
      <div>
        <div class="product-full-desc">
          <h3><?php print t('Mô tả chi tiết sản phẩm')?></h3>
          <?php print render($content['body']); ?>
        </div>
        <div class="product-shipping">
        </div>
      </div>
    </div>
  </div>

  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
  </div>
</article>