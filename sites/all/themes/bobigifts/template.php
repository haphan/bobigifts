<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

class bobigifts_theme_container  extends alpha_theme_container {
    public $sections  = array(
        'header' => 'Header',
        'content' => 'Content',
        'explorer' => 'Explorer',
        'footer' => 'Footer',
    );
}

function bobigifts_breadcrumb($variables) {
    $breadcrumb = $variables['breadcrumb'];

    if (!empty($breadcrumb)) {
        // Provide a navigational heading to give context for breadcrumb links to
        // screen-reader users. Make the heading invisible with .element-invisible.
        $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

        $output .= '<div class="breadcrumb">' . implode('<span> » </span>', $breadcrumb) . '</div>';
        return "<div class='breadcrumb-wrapper'>{$output}</div>";
    }
}


function bobigifts_form_alter(&$form, &$form_state, $form_id)
{

    //Change label of subscription form
    if('simplenews_block_form_1' == $form_id)
    {
        $form['submit']['#value'] = 'Đăng kí';
    }

}

function bobigifts_preprocess_html(&$variables) {
    drupal_add_js(drupal_get_path('theme', 'bobigifts') . '/vendor/lean-slider/lean-slider.js', array(
            'scope' => 'header',
            'weight' => '15'
        ));
    drupal_add_css(drupal_get_path('theme', 'bobigifts') . '/vendor/lean-slider/lean-slider.css');
}