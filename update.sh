#Enabling new modules if any
drush en jquery_update -y

#Clean drush cache
drush cc drush

#Reverting features
drush fra -y

#Executing pending db updates
drush updb